# ctg-txt

Categorising text with a machine learning model trained with Wikipedia

## Useful links
- Unity Game standalone download: [Google Drive](https://drive.google.com/drive/folders/1aDG5rhK1mgp9_oEb7zzpExL_BIJ_jrCn?usp=sharing)

- Wikipedia:  
    - [wikimedia dumps](https://dumps.wikimedia.org/enwiki/20200620/)  
    - [tutorial](https://towardsdatascience.com/wikipedia-data-science-working-with-the-worlds-largest-encyclopedia-c08efbac5f5c)  
- [Git - The Simple Guide](https://rogerdudler.github.io/git-guide/)

- Pitch presentation: [Google Slides: tiny.cc/ctg-txt](http://tiny.cc/ctg-txt) (request edit rights for your accounts)
- Technical report: [Overleaf](https://www.overleaf.com/read/gymmtqgwvfdb) (request edit rights for your accounts)