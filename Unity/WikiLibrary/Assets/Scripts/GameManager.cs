﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject libraryPrefab;
    public Transform libraries;
    void Start ()
    {
        generateLibraries();
        setPlayerPos();
    }

    private void generateLibraries()
    {
        //GameObject test_library = Instantiate(libraryPrefab, libraries.position, libraries.rotation, libraries);
        //test_library.GetComponent<LibraryProperties>().LibName = "Test successful";
        int numberOfLibraries = ActiveLibrary.Categories.Count + 1; // + main
        float radius = 150f;



        for (int i = 0; i < ActiveLibrary.Categories.Count; i++)
        {
            string category = ActiveLibrary.Categories[i];

            float angle = ((i+1) * Mathf.PI * 2 / numberOfLibraries) + (Mathf.PI / 2);
            float x = Mathf.Cos(angle) * radius;
            float z = Mathf.Sin(angle) * radius;
            Vector3 pos = libraries.position + new Vector3(x, 3f, z);
            Quaternion rot = Quaternion.Euler(0, -angle * Mathf.Rad2Deg + 90 , 0);
            
            GameObject library = Instantiate(libraryPrefab, pos, rot, libraries);
            library.GetComponent<LibraryProperties>().LibName = category;
        }
    }

    private void setPlayerPos()
    {

    }
}
