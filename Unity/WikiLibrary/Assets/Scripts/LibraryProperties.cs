﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LibraryProperties : MonoBehaviour
{
    public TMP_Text board;
    public string LibName;

    void Start ()
    {
        board.text = LibName;
    }

    public void EnterLibrary()
    {
        if (LibName == "Main Building")
        {
            SceneManager.LoadScene("Main");
        }
        else
        {
            ActiveLibrary.Category = LibName;
            Debug.Log(ActiveLibrary.Category);
            SceneManager.LoadScene("Library");
        }
    }
}
