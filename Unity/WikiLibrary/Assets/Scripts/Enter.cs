﻿using UnityEngine;

public class Enter : MonoBehaviour
{
    public LibraryProperties libraryProperties;

    void OnTriggerEnter()
    {
        Debug.Log("Door collision");
        libraryProperties.EnterLibrary();
    }
}
