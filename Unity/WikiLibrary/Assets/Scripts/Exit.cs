﻿using UnityEngine;

public class Exit : MonoBehaviour
{
    public CharacterController player;
    public LibraryManager libraryManager;

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Door collision");
        if (other == player)
        {
            libraryManager.ExitLibrary();
        }
    }
}
