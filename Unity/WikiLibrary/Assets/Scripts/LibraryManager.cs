﻿using System.ComponentModel;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LibraryManager : MonoBehaviour
{
    public Image GuardianGraphShelf;
    public Image RedditGraphShelf;
    public TMP_Text LibraryTitle;

    void Start()
    {
        string category = ActiveLibrary.Category;
        LibraryTitle.text = category;
        if (category == "Main Building")
        {

        } else
        {
            GuardianGraphShelf.sprite = Resources.Load<Sprite>("Guardian Graphs/" + category);
            RedditGraphShelf.sprite = Resources.Load<Sprite>("Reddit Graphs/" + category);
        }
    }

    public void ExitLibrary()
    {
        SceneManager.LoadScene("World");
    }
}
