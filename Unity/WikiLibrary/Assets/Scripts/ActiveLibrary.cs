﻿using System.Collections.Generic;

public static class ActiveLibrary
{
    public static string Category { get; set; } = "Main Building";
    public static List<string> Categories { get; } = new List<string>(new string[] {
        "Art and design",
        "Books",
        "Business",
        "Environment",
        "Fashion",
        "Film",
        "Food",
        "Football",
        "Games",
        "Law",
        "Music",
        "Politics",
        "Science",
        "Sport",
        "Technology",
        "Television _ radio",
        "Travel"
    });
}
