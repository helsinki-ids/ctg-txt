# coding: utf-8

# Author:
# -----------
# Copyright (C) 2017 Bill Thompson (biltho@mpi.nl)
#
# Modified 2020 by Matej Zidek
#
#
# Description:
# -----------
# Parses a Wikimedia sql dump of the page table (e.g. avwiki-latest-page.sql.gz) for a given wiki.
# Dumps hosted at e.g. "https://dumps.wikimedia.org/{0}wiki/latest/{0}wiki-latest-page.sql.gz".format(language_iso)
#
#
# Usage:
# -----------
# python main.py -f ../WikiData/enwiki-latest-page.sql.gz
#
#
# Returns:
# -----------
# Writes out a csv with (page_id, paget_title) columns of pages with "page_namespace"=14 (Category pages)
#
# Notes:
# -----------
# Here at the fields in the page records:
#
# COLUMNS = ["page_id", "page_namespace", "page_title", "page_restrictions", "page_counter",
#           "page_is_redirect", "page_is_new", "page_random", "page_touched", "page_links_updated",
#           "page_latest", "page_len", "page_no_title_convert", "page_content_model", "page_lang"]
#
# This script extracts "page_id", "page_namespace", "page_title", and "page_len", but could easily be adapted to extract any field
# by adding to the indexing in lines 76 - 77
#

import logging
import argparse
import gzip
import pandas as pd

from time import strftime
TIMESTAMP = strftime("%Y-%m-%d__%H-%M-%S")

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)


def parse(filename):
    logging.info("Pre-processing > Attempting parse on: {0}".format(filename))

    page_ids, page_titles = [], []

    if filename.endswith(".sql.gz"):
        new_filename = filename[:-len('.sql.gz')] + '-parsed.csv'
    else:
        new_filename = filename + "-parsed.csv"
    open(new_filename, 'w+').close()

    logging.info("Pre-processing > Unzipping: {0}".format(filename))
    with gzip.open(filename, 'r') as f:
        logging.info("Pre-processing > Unzip Success")

        logging.info(
            "Parser > Looping over file and appending parsed data to {0}".format(new_filename))

        for line in f:

            if line.startswith(b'INSERT INTO `page` VALUES'):
                line = str(line)
                entries = line[29:].split('),(')

                for entry in entries:
                    fields = entry.strip('(').strip(')').split(',')
                    if fields[1] != 14: # not a Category page
                        continue
                    print(fields[1])
                    page_ids.append(fields[0])
                    page_titles.append(fields[2])
                print(len(entries))
                results = pd.DataFrame(dict(page_id=page_ids, page_title=page_titles))
                print(results)
                return
                results.to_csv(new_filename, mode="a+",
                                index=False, encoding='utf-8')
                page_ids, page_titles = [], []

    logging.info("Parser > Parse Complete")

    logging.info("Writing dataframe out to: {0} complete".format(new_filename))

    logging.info("JOB COMPLETE.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parse a Wikimedia page dump into a csv of ("page_id", "page_title") tuples taking onle pages with "page_namespace"=14 (Category pages).')
    parser.add_argument('-f', '--filename')
    args = parser.parse_args()
    parse(args.filename)
